package regexp;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Funciones {

	public static void main(String[] args) {
		String texto;
		
		System.out.println("TELEFONOS");
		System.out.println("---------");
		texto = "Celular: (011) 5783-1290 / Laboral: (011)4321-9876";
		encontrarTelefonos(texto);
		System.out.println();

		System.out.println("DIRECCIONES DE MAIL");
		System.out.println("-------------------");
		String[] mails = Textos.listarMails().split("\\s*;\\s*");
		Arrays.stream(mails)
			.forEach(m -> System.out.println(m+": "+(esMailValido(m)?"":"no ")+"es valido"));
		System.out.println();

		System.out.println("JOB QUE PROVOCO EL ERROR");
		System.out.println("------------------------");
		String arcLog = ".\\src\\datos\\ejemploLogOracle.txt";
		texto = Textos.leerArchivo(arcLog);
		System.out.println(getErrorJob(texto));
		System.out.println();
	
	}

	public static void encontrarTelefonos(String texto) {
		Pattern pattern = Pattern.compile("\\(011\\) ?[1-9]\\d{3}-\\d{4}");
		Matcher matcher = pattern.matcher(texto);
		while (matcher.find())
			System.out.println(matcher.group());
	}

	public static boolean esMailValido(String dirMail) {
		String formato = "\\w+(\\.\\w+)*@\\w+(\\.\\w+)+";
//		return Pattern.compile(formato).matcher(dirMail).find();
		return Pattern.matches(formato, dirMail);
		}

	public static int getErrorJob(String log) {
		String expReg = "ORA-12012: error on auto execute of job (\\d+)";
		Matcher matcher = Pattern.compile(expReg,Pattern.CASE_INSENSITIVE).matcher(log);
		
		if (!matcher.find())
			throw new IllegalArgumentException("Patron de busqueda no encontrado");
		
		return Integer.parseInt(matcher.group(1));
	}

	public static String[] extraerPalabras(String frase) {
//			return Pattern.compile("\\s").split(frase);
			return frase.split("\\s");
		}

//	public static void imprimirArreglo(Object[] arreglo) {
//		Arrays.stream(arreglo).forEach(System.out::println);
//	}

}
