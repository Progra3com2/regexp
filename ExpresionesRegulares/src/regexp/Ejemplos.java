package regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ejemplos {

	public static void main(String[] args) {
		String cadena = "[bcr ]at";
		String texto = "There are Bats, cats and Rats at the end";
		
		buscarTodas(cadena, texto);
	}

	public static void buscarCadena(String patron, String texto) {
		Pattern pattern = Pattern.compile(patron);
		Matcher matcher = pattern.matcher(texto);
		
		if (matcher.find())
			System.out.println("Encontré " + matcher.group()
			+ " comenzando en " + matcher.start()
			+ " y terminando en " + matcher.end());
		else
			System.out.println("String no encontrado");
	}
	
	public static void buscarTodas(String patron, String texto) {
		Pattern pattern = Pattern.compile(patron);
		Matcher matcher = pattern.matcher(texto);
		
		while (matcher.find())
			System.out.println("Encontré " + matcher.group()
			+ " comenzando en " + matcher.start()
			+ " y terminando en " + matcher.end());
		System.out.println("Fin de la búsqueda");
	}

}
