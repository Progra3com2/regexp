package regexp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Textos {

	public static void main(String[] args) {
	
		// Ejemplo 1 de la presentacion
		String urlGoogle = "www.google.com";
		System.out.println("EJEMPLO 1");
		System.out.println("---------");
		System.out.println(pingURL(urlGoogle));
		System.out.println();
		
		// Ejemplo 2 de la presentacion
		String arcLog = ".\\src\\datos\\ejemploLogOracle.txt";
		System.out.println("EJEMPLO 2");
		System.out.println("---------");
		System.out.println(leerArchivo(arcLog));
		System.out.println();

		// Ejemplo 3 de la presentacion
		String dirWindows = "C:\\Windows";
		System.out.println("EJEMPLO 3");
		System.out.println("---------");
		System.out.println(listarDir(dirWindows));
		System.out.println();

		// Ejemplo 4 de la presentacion
		System.out.println("EJEMPLO 4");
		System.out.println("---------");
		System.out.println(listarMails());
		System.out.println();

	}

	public static String pingURL(String url) {
		String line;
		StringBuilder output = new StringBuilder();
		try {
			Process p = Runtime.getRuntime().exec("ping "+url);
			InputStreamReader reader = new InputStreamReader(p.getInputStream());
			BufferedReader input = new BufferedReader(reader);
			while ((line = input.readLine()) != null)
				output.append(line).append('\n');
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return output.toString();
	}

	public static String leerArchivo(String nomArchivo) {
		StringBuilder output = new StringBuilder();
		try {
			FileInputStream fis;
			fis = new FileInputStream(nomArchivo);
			Scanner scanner = new Scanner(fis);
			while (scanner.hasNextLine())
				output.append(scanner.nextLine()).append('\n');
			scanner.close();		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output.toString();
	}

	public static String listarDir(String dir) {
		StringBuilder output = new StringBuilder();
		File[] files = new File(dir).listFiles();
		if (files!=null)
			for (File file : files) {
				if (file.isFile())
					output.append(file.getName()).append('\n');
			}
		return output.toString();
	}

	public static String listarMails() {
		return "dbertacc@campus.ungs.edu.ar; daniel.bertaccini@gmail.com"
			 + "; noesvalido@gmail; tampoco..este@gmail.com";
	}
}
